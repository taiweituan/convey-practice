import orders from './data/orders.json';
import shipments from './data/shipments.json';
import { Request, Response } from 'express';
/**
 * Sort events by shipment Date in ascending order (latest to first)
 * @param shipment
 * @returns
 */
function sortShipmentEvents(shipment: Shipment) {
  return shipment.events.sort((a, b) => {
    const aTimestamp = new Date(a.createdDateTime).getTime();
    const bTimestamp = new Date(b.createdDateTime).getTime();
    return aTimestamp <= bTimestamp ? 1 : -1;
  });
}

function getShipmentDetail(shipment: Shipment) {
  return { ...shipment, events: sortShipmentEvents(shipment) };
}

export const testController = (req: any, res: { send: (arg0: string) => void }) => {
  console.log('Looking Good!');
  res.send('');
};

export const getOrderController = (req: Request, res: Response) => {
  const { orderNum } = req.query;
  const matchedOrder = orders.filter((order: Order) => order.orderNumber === orderNum);
  // fail fast
  if (!matchedOrder.length) {
    res.status(404);
    res.send('Order Not Found');
  }

  // This part belongs to Database, but didn't have time
  const { shipments: shipmentIds } = matchedOrder[0];
  const orderShipmentSummary = shipmentIds.map<Shipment>((id: string) => {
    // Find matching shipment ID
    for (let shipment of shipments as Shipment[]) {
      if (shipment.id === id) {
        return getShipmentDetail(shipment);
      }
    }
    // return empty if not found
    return {} as Shipment;
  });

  const totalItemsInOrder = orderShipmentSummary.reduce((accu, curr: Shipment) => {
    if (curr['quantity'] === undefined) {
      return accu;
    }
    return accu + curr['quantity'];
  }, 0);

  res.send({ totalItemsInOrder, orderShipmentSummary });
};

export const getTrackingController = (req: Request, res: Response) => {
  const { trackingNumber } = req.query;
  let result = {};

  for (let shipment of shipments as Shipment[]) {
    if (shipment.id === trackingNumber) {
      result = getShipmentDetail(shipment);
    }
  }

  if (Object.keys(result).length) {
    res.send(result);
  } else {
    res.status(404);
    res.send('Tracking Details Not Found');
  }
};

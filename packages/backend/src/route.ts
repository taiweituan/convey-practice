import express, { Express } from 'express';
import { getOrderController, testController, getTrackingController } from './controller';

const router = express.Router();

router.get('/', (req, res) => res.send('Server is up!'));
router.get('/test', testController);
router.get('/order', getOrderController);
router.get('/tracking', getTrackingController);

export default function initRoutes(app: Express) {
  app.use('/api/', router);
}

interface Order {
  orderNumber: string;
  id: string;
  shipments: string[];
}

interface ShipmentEvent {
  status: string;
  statusDesc: string;
  createdDateTime: string;
  city?: string;
  state?: string;
}
interface Shipment {
  carrier: string;
  dates: {
    estimatedDeliveryDate: string;
  };
  description: string;
  events: ShipmentEvent[];
  trackingNumber: string;
  id: string;
  orderNumber: string;
  quantity?: number;
  referenceNumber: string;
  status: string;
}
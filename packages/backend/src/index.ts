import cors from 'cors';
import express from 'express';
import http from 'http';
import initRoutes from './route';

const PORT = 4000;
const app = express();
const server = new http.Server(app);

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
initRoutes(app);

server.listen(PORT, () => console.log(`🚀 Server ready at http://localhost:${PORT}`));

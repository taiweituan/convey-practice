"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getTrackingController = exports.getOrderController = exports.testController = void 0;
var orders_json_1 = __importDefault(require("./data/orders.json"));
var shipments_json_1 = __importDefault(require("./data/shipments.json"));
/**
 * Sort events by shipment Date in ascending order (latest to first)
 * @param shipment
 * @returns
 */
function sortShipmentEvents(shipment) {
    return shipment.events.sort(function (a, b) {
        var aTimestamp = new Date(a.createdDateTime).getTime();
        var bTimestamp = new Date(b.createdDateTime).getTime();
        return aTimestamp <= bTimestamp ? 1 : -1;
    });
}
function getShipmentDetail(shipment) {
    return __assign(__assign({}, shipment), { events: sortShipmentEvents(shipment) });
}
var testController = function (req, res) {
    console.log('Looking Good!');
    res.send('');
};
exports.testController = testController;
var getOrderController = function (req, res) {
    var orderNum = req.query.orderNum;
    var matchedOrder = orders_json_1.default.filter(function (order) { return order.orderNumber === orderNum; });
    // fail fast
    if (!matchedOrder.length) {
        res.status(404);
        res.send('Order Not Found');
    }
    // This part belongs to Database, but didn't have time
    var shipmentIds = matchedOrder[0].shipments;
    var orderShipmentSummary = shipmentIds.map(function (id) {
        // Find matching shipment ID
        for (var _i = 0, _a = shipments_json_1.default; _i < _a.length; _i++) {
            var shipment = _a[_i];
            if (shipment.id === id) {
                return getShipmentDetail(shipment);
            }
        }
        // return empty if not found
        return [];
    });
    var totalItemsInOrder = orderShipmentSummary.reduce(function (accu, curr) {
        if (!curr['quantity']) {
            return accu;
        }
        return accu + curr['quantity'];
    }, 0);
    res.send({ totalItemsInOrder: totalItemsInOrder, orderShipmentSummary: orderShipmentSummary });
};
exports.getOrderController = getOrderController;
var getTrackingController = function (req, res) {
    var trackingNumber = req.query.trackingNumber;
    var result = {};
    for (var _i = 0, _a = shipments_json_1.default; _i < _a.length; _i++) {
        var shipment = _a[_i];
        if (shipment.id === trackingNumber) {
            result = getShipmentDetail(shipment);
        }
    }
    if (Object.keys(result).length) {
        res.send(result);
    }
    else {
        res.status('404');
        res.send('Tracking Details Not Found');
    }
};
exports.getTrackingController = getTrackingController;
//# sourceMappingURL=controller.js.map
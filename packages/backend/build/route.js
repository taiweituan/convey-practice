"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var controller_1 = require("./controller");
var router = express_1.default.Router();
router.get('/', function (req, res) { return res.send('Server is up!'); });
router.get('/test', controller_1.testController);
router.get('/order', controller_1.getOrderController);
router.get('/tracking', controller_1.getTrackingController);
function initRoutes(app) {
    app.use('/', router);
}
exports.default = initRoutes;
//# sourceMappingURL=route.js.map
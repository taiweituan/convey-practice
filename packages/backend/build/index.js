"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var cors_1 = __importDefault(require("cors"));
var express_1 = __importDefault(require("express"));
var http_1 = __importDefault(require("http"));
var route_1 = __importDefault(require("./route"));
var PORT = 4000;
var app = (0, express_1.default)();
var server = new http_1.default.Server(app);
app.use((0, cors_1.default)());
app.use(express_1.default.json());
app.use(express_1.default.urlencoded({ extended: false }));
(0, route_1.default)(app);
server.listen(PORT, function () { return console.log("\uD83D\uDE80 Server ready at http://localhost:".concat(PORT)); });
//# sourceMappingURL=index.js.map
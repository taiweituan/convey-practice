import './styles.scss';
import NavBar from './components/navbar/Navbar';
import { OrderDetailsPage } from './components/orderDetails/OrderDetails';
export default function App() {
  return (
    <div className="App">
      <NavBar />
      <OrderDetailsPage />
    </div>
  );
}

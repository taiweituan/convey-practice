export interface Order {
  orderNumber: string;
  id: string;
  shipments: string[];
}

export interface ShipmentEvent {
  status: string;
  statusDesc: string;
  createdDateTime: string;
  city?: string;
  state?: string;
}

export interface Tracking {
  carrier: string;
  dates: {
    estimatedDeliveryDate: string;
  };
  description: string;
  events: ShipmentEvent[];
  trackingNumber: string;
  id: string;
  orderNumber: string;
  quantity?: number;
  referenceNumber: string;
  status: string;
}

export interface OrderDetail {
  totalItemsInOrder: number;
  orderShipmentSummary: Tracking[];
}

export type TrackingStatus = 'schedule' | 'in_transit';

export interface TrackingDetailsProps {
  carrier: string;
  events: ShipmentEvent[];
}
export interface OrderOverviewDetailProps {
  date: string;
  quantity: number;
  referenceNumber: string;
  description: string;
  id: string;
  handleTrackDetailsFunc: (trackNumber?: string) => void;
  setIsShowFullOrder: (value: React.SetStateAction<boolean>) => void;
}

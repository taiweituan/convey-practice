import { useState } from 'react';
import './navbar.scss';
import Logo from '../../assets/deerreed_logo.png';

const NAVBAR_OPTIONS = ['Men', 'Women', 'Kids', 'Home', 'Sale', 'Get Help'];

export default function Navbar(): JSX.Element {
  const [isToggled, setIsToggled] = useState(false);

  const handleToggle = () => {
    setIsToggled(!isToggled);
  };
  return (
    <nav className="navbar navbar-expand-lg py-3 navbar-light">
      <div className="container">
        <a
          className="navbar-brand"
          href="https://www.getconvey.com"
          target="_blank"
          rel="noreferrer"
        >
          <img className="navbar__logo" src={Logo} />
        </a>
        <button onClick={handleToggle} className="navbar__toggler navbar-toggler" type="button">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div
          className={`collapse navbar-collapse ${isToggled ? `mobile-mode` : ``}`}
          id="navbarText"
        >
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            {NAVBAR_OPTIONS.map((option, i) => {
              return (
                <li key={i} className="nav-item">
                  <a className="nav-link" aria-current="page" href="#">
                    {option}
                  </a>
                </li>
              );
            })}
          </ul>
        </div>
      </div>
    </nav>
  );
}

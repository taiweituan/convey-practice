import promoImg from '../../assets/img_Promo.png';

export function PromoSection() {
  return (
    <div className="col-lg-6">
      <div className="promo">
        <a href="https://www.getconvey.com">
          <img src={promoImg} />
        </a>
      </div>
    </div>
  );
}

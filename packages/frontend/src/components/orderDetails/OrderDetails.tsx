import { useState } from 'react';
import { OrderDetail, ShipmentEvent, Tracking, TrackingStatus } from '../../types/types';
import { PromoSection } from './PromoSection';
// assets and style
import './orderDetails.scss';
import { ReactComponent as PickupIcon } from '../../assets/ic_pickups.svg';
import { TrackingDetails } from './TrackingDetails';
import { OrderOverview } from './OrderOverview';
import { handleEstimateDeliveryDate } from '../../utilities/index';
import { useFetch } from '../../utilities/customHook/useFetch';

const TRACKING_STATUS = {
  schedule: 'Schedule',
  in_transit: 'In Transit',
};

export function OrderDetailsPage(): JSX.Element {
  const [isShowFullOrder, setIsShowFullOrder] = useState(false);
  const [trackingNumber, setTrackingNumber] = useState('f10a27fa-d4bd-4790-a3b3-b41d4c5c4071');
  const { data: orderDetails, isLoading: isLoadingOrders } = useFetch<OrderDetail>(
    '/order?orderNum=336951301',
  );
  const { data: trackingDetailsData, isLoading: isLoadingTracking } = useFetch<Tracking>(
    `/tracking?trackingNumber=${trackingNumber}`,
  );
  const isLoading = isLoadingOrders && isLoadingTracking;

  const handleTrackDetailsFunc = (trackNumber = '') => {
    setTrackingNumber(trackNumber);
  };

  return isLoading ? (
    <div>Loading</div>
  ) : (
    <div className="container order-details">
      <div className="items-text">
        This shipment includes {trackingDetailsData?.quantity || 0} of your
        {orderDetails?.totalItemsInOrder} items.&nbsp;
        <a
          onClick={() => {
            setIsShowFullOrder(true);
          }}
        >
          <b>See Full Order</b>
        </a>
      </div>

      <div className="row justify-content-center">
        <div className="col-lg-6">
          <div className="shipment-status" id="shipment-status">
            <div className="transit-status p-3">
              <div className="pickup-icon m-3">
                <PickupIcon />
              </div>
              <p>
                {TRACKING_STATUS[trackingDetailsData?.events[0].status as TrackingStatus] ||
                  'Unknown'}
              </p>
              <p>
                Estimated Delivery:{' '}
                <b>
                  {handleEstimateDeliveryDate(trackingDetailsData?.dates.estimatedDeliveryDate)}
                </b>
              </p>
            </div>

            <hr />
            <TrackingDetails
              carrier={trackingDetailsData?.carrier || 'unknown'}
              events={trackingDetailsData?.events as ShipmentEvent[]}
            />
          </div>
        </div>
        <PromoSection />
      </div>

      <div className="row justify-content-center">
        <button
          className="btn btn-lg btn-primary see-other-btn"
          onClick={() => {
            setIsShowFullOrder(true);
          }}
        >
          SEE ALL ITEMS IN YORU ORDER
        </button>
      </div>

      {isShowFullOrder ? (
        <OrderOverview
          setIsShowFullOrder={setIsShowFullOrder}
          order={orderDetails as OrderDetail}
          handleTrackDetailsFunc={handleTrackDetailsFunc}
        />
      ) : (
        <></>
      )}
    </div>
  );
}

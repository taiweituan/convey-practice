import { OrderDetail } from '../../types/types';
import './OrderOverview.scss';
import { ReactComponent as CloseIconSvg } from '../../assets/ic_close.svg';
import { OrderOverviewDetail } from './OrderOverviewDetail';

export function OrderOverview(props: {
  order: OrderDetail;
  handleTrackDetailsFunc: (trackNumber?: string) => void;
  setIsShowFullOrder: React.Dispatch<React.SetStateAction<boolean>>;
}) {
  const { orderShipmentSummary } = props.order as OrderDetail;

  return (
    <div id="order-overview" className="order-overview">
      <div
        className="order-overview__overlay"
        onClick={() => props.setIsShowFullOrder(false)}
      ></div>
      <div className="order-overview__popup">
        <div className="order-overview__popup-title">
          <h3>Order Overview</h3>
          <a href="#" onClick={() => props.setIsShowFullOrder(false)}>
            <CloseIconSvg />
          </a>
        </div>
        <div className="order-overview__body">
          {orderShipmentSummary.map((summary, i) => {
            return (
              <OrderOverviewDetail
                key={i}
                id={summary.id}
                date={summary.dates.estimatedDeliveryDate}
                quantity={summary.quantity || 0}
                referenceNumber={summary.referenceNumber}
                description={summary.description}
                handleTrackDetailsFunc={props.handleTrackDetailsFunc}
                setIsShowFullOrder={() => props.setIsShowFullOrder(false)}
              />
            );
          })}
        </div>
      </div>
    </div>
  );
}

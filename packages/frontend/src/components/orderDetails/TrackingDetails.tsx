import { TrackingDetailsProps } from '../../types/types';
import './TrackingDetails.scss';
import FedexPng from '../../assets/img_Fedex.png';
import UpsPng from '../../assets/img_ups.png';
import dayjs from 'dayjs';

function CarrierImage(props: { carrier: string }) {
  switch (props.carrier) {
    case 'FedEx':
      return <img src={FedexPng} />;
    case 'UPS':
      return <img src={UpsPng} />;
    default:
      return <img src={UpsPng} />;
  }
}
function handleLocation(city = '', state = '') {
  return `${city ? `${city}, ${state}` : `${state}`}`;
}

export function TrackingDetails(props: TrackingDetailsProps) {
  const { events = [], carrier = '' } = props as TrackingDetailsProps;

  return (
    <div className="tracking-details" id="tracking-details">
      {events.map((evt, i) => {
        return (
          <div key={i}>
            {/* <p className="tracking-details__day">Today</p> */}
            <div className="container tracking-details__pills">
              <div className="col-2 col-lg-3">
                <CarrierImage carrier={carrier} />
                <small className="tracking-details__time">
                  {dayjs(evt.createdDateTime).format('h:mm A')}
                </small>
              </div>
              <div className="col-10 col-lg-9">
                <div className="contatiner">
                  <div className="row">
                    <div className="col-12 col-lg-6">
                      <b>In Transit</b>
                    </div>
                    <div className="col-12 col-lg-6 tracking-details__location">
                      {handleLocation(evt.city, evt.state)}
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-lg-12">
                      <small>At local FedEx facility</small>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        );
      })}
    </div>
  );
}

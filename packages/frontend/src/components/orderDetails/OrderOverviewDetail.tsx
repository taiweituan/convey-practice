import { OrderOverviewDetailProps } from '../../types/types';
import { handleEstimateDeliveryDate } from '../../utilities';

export function OrderOverviewDetail(props: OrderOverviewDetailProps) {
  const { date, quantity, referenceNumber, description, id, handleTrackDetailsFunc } =
    props as OrderOverviewDetailProps;

  return (
    <div className="order-overview__body__items">
      <div className="order-overview__body__estimate-delivery">ESTIMATED DELIVERY</div>
      <div className="order-overview__body__date">{handleEstimateDeliveryDate(date)}</div>
      <div className="order-overview__body__details">
        <table>
          <tbody>
            <tr>
              <td>Quantity</td>
              <td>{quantity}</td>
            </tr>
            <tr>
              <td>Ref #</td>
              <td>{referenceNumber}</td>
            </tr>
            <tr>
              <td>Description</td>
              <td>{description}</td>
            </tr>
          </tbody>
        </table>
      </div>
      <button
        className="btn btn-large btn-primary order-overview__btn-see-tracking-details"
        onClick={() => {
          handleTrackDetailsFunc(id);
          props.setIsShowFullOrder(false);
        }}
      >
        SEE TRACKIGN DETAILS
      </button>
    </div>
  );
}

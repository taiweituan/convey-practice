import { useState, useEffect } from 'react';

const API_URL = `http://localhost:4000/api`;

export function useFetch<T>(urlPath = '') {
  const [isLoading, setIsLoading] = useState<boolean>(true);
  // eslint-disable-next-line
  const [data, setData] = useState<T>();

  useEffect(() => {
    if (!urlPath) return;

    const fetchData = async () => {
      const response = await fetch(`${API_URL}${urlPath}`);
      const data = await response.json();

      setData(data);
      setIsLoading(false);
    };

    fetchData();
  }, [urlPath]);

  return { data, isLoading };
}

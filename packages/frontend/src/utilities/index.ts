import dayjs from 'dayjs';

export function handleEstimateDeliveryDate(dateString = ''): string {
  return dayjs(dateString).format('dddd, MMM D');
}

PROJECT=convey-proctice
COMPOSE=docker-compose --file ./docker-compose.yml

test:
	echo 'make file invoked!'

pull:
	$(COMPOSE) pull

build: pull
	$(COMPOSE) build

up:
	$(COMPOSE) up --remove-orphans

down:
	$(COMPOSE) down

kill:
	$(COMPOSE) kill